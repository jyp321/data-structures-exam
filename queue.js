function Queue() {
    this.collection = [];

    // This method will print the collection in the console.
    this.print = function () {
        return this.collection
    };

    // This method will add an element to the end of the queue.
    this.enqueue = function (value) {
        this.collection.push(value);
        return this.collection;
    };

    // This method will remove the first item in the collection.
    this.dequeue = function () {
        this.collection.shift();
        return this.collection;
    };

    // This method will return the first element of the queue.
    this.front = function () {
        if (this.collection.length == 0) {
            return null
        } else {
            return this.collection[0]
        }
    };

    // This method will return the number of elemens in the queue.
    this.size = function () {
        return this.collection.length
    };

    // This method will check if the queue is empty or not.
    this.isEmpty = function () {
        if (this.collection.length == 0) {
            return true
        } else {
            return false
        }
    };
}

let myFirstQueue = new Queue()
myFirstQueue.enqueue(['John Doe', 2]);
myFirstQueue.enqueue(['Jane Doe', 3]);
myFirstQueue.enqueue(['John Smith', 1]);
myFirstQueue.enqueue(['John Smith', 1]);
//expected final output from priority queue: ["John Smith", "Jane Smith", "John Doe", "Jane Doe"]

module.exports = Queue;
